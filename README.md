# Welcome

This is a repository to track the songs I need to record, edit, and public on my [YouTube channel @JuvarAbrera](https://www.youtube.com/@JuvarAbrera)

# Procedures
### Record Audio
**Software**: Samsung Voice Recorder \
**Quality**: 256kbps, 48kHz Stereo

### Record Video
**Software**: Windows Camera \
**Hardware**: Logitech Brio 4K \
**Quality**: 3840x2160 (4K) 30fps

### Audio Post Production
**Software**: Adobe Audition \
**Effects**: Single-band Compressor, DeEsser, Vocal Enhancer, Normalize, Denoise, Echo \
**Quality**: 48kHz Stereo 32-bit FLAC format

### Video Post Production
**Software**: CyberLink PowerDirector 20 \
**Effects**: Minor color adjustments \
**Quality**: H.265/HEVC MPEG-4 4K 3840x2160 30p (37Mbps) 
